void Swap(int *arr, int pos1, int pos2) {
  if (pos1 != pos2) {
    arr[pos1] += arr[pos2];
    arr[pos2] = arr[pos1] - arr[pos2];
    arr[pos1] -= arr[pos2];
  }
}

void Sort(int *arr, int l, int r) {
  if (r - l > 9) {  //使用合并排序
    int mid = (l + r) / 2;
    Sort(arr, l, mid);
    Sort(arr, mid + 1, r);
    int *temp = new int[r - l + 1];
    int pos = 0;
    int i = l, j = mid + 1;
    while (i <= mid && j <= r) {
      if (arr[i] <= arr[j]) temp[pos++] = arr[i++];
      else temp[pos++] = arr[j++];
    }
    while (i <= mid) temp[pos++] = arr[i++];
    while (j <= r) temp[pos++] = arr[j++];
    pos = 0;
    while (pos < r - l + 1) {
      arr[l + pos] = temp[pos];
      pos++;
    }
    delete[] temp;
  } else if (r > l) {  //使插入排序
    for (int i = l + 1; i <= r; i++) {
      for (int j = i; j > l; j--) {
        if (arr[j] < arr[j - 1]) Swap(arr, j, j - 1);
        else break;
      }
    }
  }
}