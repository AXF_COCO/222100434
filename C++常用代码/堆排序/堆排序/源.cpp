void Swap(int *arr, int pos1, int pos2) {
  if (pos1 != pos2) {
    arr[pos1] += arr[pos2];
    arr[pos2] = arr[pos1] - arr[pos2];
    arr[pos1] -= arr[pos2];
  }
}

//数组为从1开始记录的数组(arr[0]不存数据)
void Sift(int *arr, int pos, int n) {
  int son = pos * 2;
  while (son <= n) {
    if (son < n && arr[son] < arr[son + 1]) son++;
    if (arr[pos] < arr[son]) Swap(arr, pos, son);
    else break;
    pos = son;
    son *= 2;
  }
}

void HeapSort(int *arr, int n) {
  for (int i = n / 2; i > 0; i--) Sift(arr, i, n);
  for (int i = n; i > 1; i--) {
    Swap(arr, 1, i);
    Sift(arr, 1, i - 1);
  }
}