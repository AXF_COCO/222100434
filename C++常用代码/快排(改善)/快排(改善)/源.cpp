#include <cstdlib>

void Swap(int *arr, int pos1, int pos2) {
  if (pos1 != pos2) {
    arr[pos1] += arr[pos2];
    arr[pos2] = arr[pos1] - arr[pos2];
    arr[pos1] -= arr[pos2];
  }
}

void Sort(int *arr, int l, int r) {
  if (r - l > 9) {  //使用快排
    int standard = arr[rand() % (r - l + 1) + l];
    int i = l, j = r;
    while (arr[i] <= standard && i < r) i++;
    while (arr[j] > standard && j > l) j--;
    while (i < j) {
      Swap(arr, i, j);
      while (arr[i] <= standard) i++;
      while (arr[j] > standard) j--;
    }
    //猜测可能会数据是多个相同的数，将随机值移动到[l, j]的右侧
    int temp = j;
    for (int cnt = 0, k = l; k < temp - cnt; k++) {
      while (arr[k] == standard && j >= k) {
        Swap(arr, k, j--);
        cnt++;
      }
    }
    Sort(arr, l, j);  //i的判断条件是<=，有可能出界，故用j
    Sort(arr, temp + 1, r);
  } else if (r > l) {  //使插入排序
    for (int i = l + 1; i <= r; i++) {
      for (int j = i; j > l; j--) {
        if (arr[j] < arr[j - 1]) Swap(arr, j, j - 1);
        else break;
      }
    }
  }
}